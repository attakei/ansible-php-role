# -*- coding:utf8 -*-


def test_installed_php_fpm(host):
    fpm_conf = host.file('/etc/php-fpm.conf')
    assert fpm_conf.exists
    fpm_www = host.file('/etc/php-fpm.d/www.conf')
    assert fpm_www.exists
