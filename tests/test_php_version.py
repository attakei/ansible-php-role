# -*- coding:utf8 -*-


def test_installed_php_version(host):
    assert host.package('php').is_installed
    php_version = host.check_output("php -v")
    assert 'PHP 5.6' in php_version
