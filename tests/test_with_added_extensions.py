# -*- coding:utf8 -*-


def test_added_extensions(host):
    php_version = host.check_output("php -m")
    assert 'mcrypt' in php_version
