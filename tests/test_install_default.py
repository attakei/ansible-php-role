# -*- coding:utf8 -*-


def test_has_dependencies(host):
    assert host.package("epel-release").is_installed
    assert host.package("remi-release").is_installed


def test_installed_php(host):
    assert host.package('php').is_installed
    php_version = host.check_output("php -v")
    assert 'PHP 7.1' in php_version
    php_version = host.check_output("php -m")
    assert 'mbstring' in php_version
    session_dir = host.file('/var/lib/php/session')
    assert session_dir.user == 'root'
    assert session_dir.group == 'apache'
    php_ini = host.file('/etc/php.ini')
    assert php_ini.exists
    assert php_ini.contains('date.timezone = Asia/Tokyo')
